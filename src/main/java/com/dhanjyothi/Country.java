package com.dhanjyothi;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.annotations.ApiModelProperty;
 
@Entity
@Table(name="country")
public class Country {
    
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @ApiModelProperty(hidden=true)
    private int id;
    private String code;
    private String name;
 
    public int getId() {
        return id;
    }
 
  
    public void setId(int id) {
        this.id = id;
    }
 
   
    public String getCode() {
        return code;
    }
 
  
    public void setCode(String code) {
        this.code = code;
    }
 
    public String getName() {
        return name;
    }
 
  
    public void setName(String name) {
        this.name = name;
    }
    
    @Override
    public String toString() {
        return String.format("Country[id=%d, code='%s', name='%s']",
                id, code, name);
    }
}