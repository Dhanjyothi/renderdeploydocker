package com.dhanjyothi;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
 public interface CountryRepository extends CrudRepository<Country,Integer>,JpaRepository<Country,Integer>{
  
}

 
 /*  @Transactional
 @Modifying
 @Query("update Country c set c.code=?1,c.name=?2 where c.id=?3")
void setCountryInfoById(String code, String name, int id);

List<Country> findByName(String name);
@Query(value="select * from country where name=?1",nativeQuery=true)
List<Country> fetchByName(String name);
 
 @Modifying
 @Transactional
 @Query("update Country c set c.code = ?1, c.name = ?2 where c.id = ?3")
 void setCountryInfoById(String code, String name, Integer userId);


List<Country> findByIdGreaterThan(int id);

List<Country> findByCodeAndName(String code, String name);


List<Country> findByName(String name);

*/