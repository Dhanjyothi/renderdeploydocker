package com.dhanjyothi;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ReferenceDataController {
  
	@Autowired
	CountryRepository dao; 
	@PostMapping("/saveData")
    public String saveCountryDetails(@RequestBody Country c) {
	    
	      dao.save(c);
	
	return "Saved Successfully";
	       } 
	@GetMapping("listCountries")
	 public List<Country> getCountryDetails() {
	    
	      return dao.findAll();
	}
}
	
	
	/*
	 @RequestMapping(value = "/listCountries")
	    public List getEmployees() {
	          List<Country> list=dao.findAll();
            return list;
	        
	    }
	 @RequestMapping("/delete/{id}")
     public String deleteCountry(@PathVariable int id) {
         dao.deleteById(id);
      return "deleted successfully";
        
     }
	 
	 @RequestMapping("/update/{code}/{name}/{id}")
	    public String updateCountry(@PathVariable String code,@PathVariable String name,@PathVariable int id) {
	     dao.setCountryInfoById(code,name,id);
	     return "Updated Successfully";
	       } 
	 @RequestMapping("/fetchbyname/{name}")
     public List<Country> getName(@PathVariable String name) {

         List<Country>   result = dao.fetchByName(name);
          return result;
      
         
     }
       @RequestMapping("/update/{code}/{name}/{id}")
    public String updateCountry(@PathVariable String code,@PathVariable String name,@PathVariable int id) {
    	   
     dao.setCountryInfoById(code,name,id);
  
//dao.saveData(1,"1234","revathi");
return "Updated Successfully";
       } 
           
      
       @RequestMapping("/fetchbyname/{name}")
       public List<Country> getName(@PathVariable String name) {

           List<Country>   result = dao.findByName(name);
   			return result;
    	 //  return null;
           
       }
       @RequestMapping("/fetch/{id}")
       public List<Country> getCountry(@PathVariable int id) {

          List<Country>   result = dao.findByIdGreaterThan(id);
   			return result;
   			//select * from country where id>1
          //return null;
       }
      
     
     @RequestMapping("/fetchdata")
      public List<Country> getCountry(@RequestParam String code,@RequestParam String name) {

           List<Country>   result = dao.findByCodeAndName(code,name);
           		return result;
    	
          
      }
     @RequestMapping(value = "/addItem",consumes = MediaType.APPLICATION_JSON_VALUE)
     public ResponseEntity<Item> addItem(@RequestBody Item item){
         itemRepo.save(item);
         return new ResponseEntity<Item>(item, HttpStatus.OK);
     }
     
     @RequestMapping("/getAllItems")
     @ResponseBody
     public ResponseEntity<List<Item>> getAllItems(){
         List<Item> items =  itemRepo.findAll();
         return new ResponseEntity<List<Item>>(items, HttpStatus.OK);
     }
 */
      
